import axios from "axios";

import IPAllocator from "./IPManager";
import ContainerManager from "./ContainerManager";
import ServiceManager from "./ServiceManager";
import IPv4 from "./IPv4";

export default class Kubelet {
  private containerManager: ContainerManager;
  private serviceManager = new ServiceManager();

  public async register() {
    const { data: subnet } = await axios.get(`http://10.0.0.1:3000/register`);
    this.containerManager = new ContainerManager(new IPAllocator(subnet));
    await this.containerManager.init();
  }

  public async runContainer(image: string): Promise<IPv4> {
    const container = await this.containerManager.run(image);
    return container.ip;
  }

  public async stopContainer(ip: string) {
    await this.containerManager.stop(ip);
  }

  public checkContainer(ip: string) {
    return this.containerManager.isRunning(ip);
  }

  public async addService(domain: string, ip: string) {
    await this.serviceManager.add(domain, ip);
  }

  public async removeService(ip: string) {
    await this.serviceManager.remove(ip);
  }

  public async destroy() {
    await this.containerManager?.destroy();
  }
}
