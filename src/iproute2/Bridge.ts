import Device from "./Device";

export default class Bridge extends Device {
  public async instantiate() {
    await this.exec(`ip link add ${this.name} type bridge`);
  }

  public async assign(device: Device) {
    await this.exec(`ip link set ${device.name} master ${this.name}`);
  }
}
