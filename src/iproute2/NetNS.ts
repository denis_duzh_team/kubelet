import { exec as _exec } from "child_process";
import { promisify } from "util";

import Device from "./Device";

const exec = promisify(_exec);

export default class NetNS {
  private _name: string;

  constructor(name: string) {
    this._name = name;
  }

  public async instantiate(pid: string) {
    await exec(`ln -sfT /proc/${pid}/ns/net /var/run/netns/${this._name}`);
  }

  public async assign(device: Device) {
    device.context = `ip netns exec ${this._name}`;
    await exec(`ip link set dev ${device.name} netns ${this._name}`);
  }

  public async setGateway(subnet: string, gatewayIP: string) {
    await exec(`ip netns exec ${this._name} ip route add ${subnet} via ${gatewayIP}`);
  }

  public async remove() {
    await exec(`ip netns del ${this._name}`);
  }

  public get name(): string {
    return this._name;
  }
}
