import { exec as _exec } from "child_process";
import { promisify } from "util";

const exec = promisify(_exec);

export default class Docker {
  public static async run(image: string): Promise<{ id: string; pid: string }> {
    const { stdout: id } = await exec(`docker run --rm -tid --network=none ${image}`);
    const { stdout: pid } = await exec(`docker inspect -f '{{.State.Pid}}' ${id}`);

    return { id: id.trim(), pid: pid.trim() };
  }

  public static async isRunning(id: string) {
    try {
      const result = await exec(`docker inspect -f '{{.State.Running}}' ${id}`);
      return Boolean(result);
    } catch {
      return false;
    }
  }

  public static async stop(id: string): Promise<void> {
    try {
      await exec(`docker stop ${id}`);
    } catch {}
  }
}
