import { exec as _exec } from "child_process";
import { promisify } from "util";

const exec = promisify(_exec);

export default abstract class Device {
  private _name: string;
  private _context: string = "";
  private _ip: string = null;

  constructor(name: string) {
    this._name = name;
  }

  public abstract async instantiate(): Promise<void>;

  protected async exec(command: string) {
    await exec(`${this._context} ${command}`);
  }

  public async remove() {
    await this.exec(`ip link del ${this._name}`);
  }

  public async up() {
    await this.exec(`ip link set ${this._name} up`);
  }

  public async setIP(ip: string, subnetBits: number) {
    this._ip = ip;
    await this.exec(`ip addr add ${ip}/${subnetBits} dev ${this._name}`);
  }

  public get name(): string {
    return this._name;
  }

  public set context(_context: string) {
    this._context = _context;
  }

  public get ip(): string {
    return this._ip;
  }
}
