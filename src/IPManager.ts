import IPv4 from "./IPv4";

export default class IPAllocator {
  private lastIP: IPv4;
  private returnedIPs: IPv4[] = [];

  constructor(subnet: string) {
    const [ip, maskBits] = subnet.split("/");

    this.lastIP = new IPv4(ip, Number(maskBits));
  }

  public nextIP(): IPv4 {
    if (this.returnedIPs.length > 0) return this.returnedIPs.shift();
    this.lastIP = new IPv4(this.lastIP.address, this.lastIP.maskBits);
    const [byte3, byte2, byte1, byte0] = this.lastIP.addressBytes;
    this.lastIP.addressBytes = [byte3, byte2, byte1, byte0 + 1];

    return this.lastIP;
  }

  public returnIP(ip: IPv4) {
    this.returnedIPs.push(ip);
  }
}
