import { NetNS, Veth, Bridge } from "./iproute2";
import Docker from "./Docker";
import IPv4 from "./IPv4";

export default class Container {
  private _id: string;
  private _ip: IPv4;
  private _veth: Veth;
  private _netNS: NetNS;
  private _image: string;

  constructor(image: string) {
    this._image = image;
  }

  public async run(_ip: IPv4, bridge: Bridge) {
    const { id, pid } = await Docker.run(this._image);

    this._veth = new Veth(id.slice(0, 13));
    await this._veth.instantiate();

    await bridge.assign(this._veth.pair);
    await this._veth.pair.up();

    const netNS = new NetNS(id);
    await netNS.instantiate(pid);
    await netNS.assign(this._veth);
    await this._veth.up();
    await this._veth.setIP(_ip.address, _ip.maskBits);
    await netNS.setGateway("default", bridge.ip);
    this._netNS = netNS;

    this._ip = _ip;
    this._id = id;
  }

  public async alive(): Promise<boolean> {
    return await Docker.isRunning(this._id);
  }

  public async stop() {
    await this._netNS.remove();
    await Docker.stop(this._id);
  }

  public get id(): string {
    return this._id;
  }

  public get veth(): Veth {
    return this._veth.pair;
  }

  public get ip(): IPv4 {
    return this._ip;
  }
}
