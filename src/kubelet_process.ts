import express from "express";
import bodyParser from "body-parser";
import { Server } from "http";
import { exec as _exec } from "child_process";
import { promisify } from "util";

const exec = promisify(_exec);

import Kubelet from "./Kubelet";

let kubelet: Kubelet;
let server: Server;

export const bootstrap = async () => {
  await exec("ip route add 10.0.0.0/8 via 10.0.0.1");

  const app = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  kubelet = new Kubelet();
  await kubelet.register();

  app.post("/containers", async (req, res) => {
    const { image } = req.body;
    const ip = await kubelet.runContainer(image);
    console.log("Created container with IP: %s", ip.address);
    res.send(ip.address);
  });

  app.delete("/containers/:ip", async (req, res) => {
    const { ip } = req.params;
    await kubelet.stopContainer(ip);
    res.send("Ok");
  });

  app.get("/containers/:ip/running", (req, res) => {
    const { ip } = req.params;
    res.send(kubelet.checkContainer(ip));
  });

  app.post("/services", async (req, res) => {
    const { domain, ip } = req.body;
    await kubelet.addService(domain, ip);
    res.send("Ok");
  });

  app.delete("/services/:ip", async (req, res) => {
    const { ip } = req.params;
    await kubelet.removeService(ip);
    res.send("Ok");
  });

  server = app.listen(3000);
};

export const cleanup = () => {
  exec("ip route del 10.0.0.0/8 via 10.0.0.1");
  kubelet?.destroy();
  server?.close();
};
