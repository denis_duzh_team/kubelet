import { bootstrap, cleanup } from "./kubelet_process";

bootstrap().catch(error => {
  console.log(error);
  process.exit(1);
});

process.on("SIGINT", cleanup);
