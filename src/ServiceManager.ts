import { promisify } from "util";
import { appendFile as _appendFile, readFile as _readFile, writeFile as _writeFile } from "fs";

const appendFile = promisify(_appendFile);
const readFile = promisify(_readFile);
const writeFile = promisify(_writeFile);

interface Service {
  domain: string;
  ip: string;
}

export default class ServiceManager {
  private services: Service[] = [];

  public async add(domain: string, ip: string) {
    await appendFile("/etc/hosts", `\n${ip} ${domain}\n`);
    this.services.push({ domain, ip });
  }

  public async remove(ip: string) {
    const file = (await readFile("/etc/hosts")).toString();
    const serviceToDelete = this.services.find(service => service.ip === ip);
    await writeFile(
      "/etc/hosts",
      file.replace(`\n${serviceToDelete.ip} ${serviceToDelete.domain}\n`, "")
    );
  }
}
