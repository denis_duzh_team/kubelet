type Address = [number, number, number, number];

export default class IPv4 {
  private _address: Address = null;
  private _maskBits: number;

  constructor(address: string, maskBits?: number) {
    this._address = address.split(".").map(Number) as Address;
    this._maskBits = maskBits;
  }

  public get addressBytes(): Address {
    return this._address;
  }

  public set addressBytes(bytes: Address) {
    this._address = bytes;
  }

  public get address(): string {
    return this._address.join(".");
  }

  public get maskBits(): number {
    return this._maskBits;
  }
}
