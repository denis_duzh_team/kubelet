import { promisify } from "util";
import { exec as _exec } from "child_process";

const exec = promisify(_exec);

import Container from "./Container";
import { Bridge } from "./iproute2";
import IPAllocator from "./IPManager";

export default class ContainerManager {
  private containers: Container[] = [];
  private bridge = new Bridge("cbr0");
  private ipManager: IPAllocator;
  private destroyed = false;

  constructor(ipManager: IPAllocator) {
    this.ipManager = ipManager;
  }

  public async init() {
    await this.bridge.instantiate();
    await this.bridge.up();
    const { address, maskBits } = this.ipManager.nextIP();
    await this.bridge.setIP(address, maskBits);
    await exec("mkdir -p /var/run/netns");

    this.checkAlive();
  }

  public async run(image: string): Promise<Container> {
    const container = new Container(image);
    await container.run(this.ipManager.nextIP(), this.bridge);

    this.containers.push(container);
    return container;
  }

  public async stop(ip: string) {
    const container = this.containers.find(container => container.ip.address === ip);
    if (container) {
      this.ipManager.returnIP(container.ip);
      await container.stop();
      this.containers.splice(this.containers.indexOf(container), 1);
    }
  }

  public isRunning(ip: string): boolean {
    const container = this.containers.find(container => container.ip.address === ip);
    return container !== undefined;
  }

  private async checkAlive() {
    await Promise.all(
      this.containers.map(
        async container => !(await container.alive()) && this.stop(container.ip.address)
      )
    );

    setTimeout(() => !this.destroyed && this.checkAlive(), 3000);
  }

  public async destroy() {
    this.destroyed = true;
    await Promise.all(this.containers.map(container => container.stop()));
    await this.bridge.remove();
  }
}
