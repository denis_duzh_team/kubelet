import Device from "./Device";

export default class Veth extends Device {
  private _pair: Veth;

  public async instantiate() {
    this._pair = new Veth(`${this.name}-p`);
    await this.exec(`ip link add ${this.name} type veth peer name ${this._pair.name}`);
  }

  public get pair(): Veth {
    return this._pair;
  }
}
